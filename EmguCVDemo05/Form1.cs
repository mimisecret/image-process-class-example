﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmguCVDemo05
{
    public partial class Form1 : Form
    {
        private Image<Bgr, byte> source;
        private Bitmap textImage;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnLoadImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog()
            {
                Filter = "Image|*.jpg;*.png|All Files|*.*"
            };
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                source = new Image<Bgr, byte>(ofd.FileName);
                pictureBox1.Image = source.Bitmap;
            }
        }

        private async void btnCaptureText_Click(object sender, EventArgs e)
        {
            Image<Gray, byte> result = source.Convert<Gray, byte>()
                .Not()
                .ThresholdBinary(new Gray(127), new Gray(255));
            VectorOfVectorOfPoint vvp = new VectorOfVectorOfPoint();
            Mat mat = new Mat();

            CvInvoke.FindContours(
                result,
                vvp,
                mat,
                Emgu.CV.CvEnum.RetrType.External,
                Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple
                );

            if (vvp.Size > 0)
            {
                label1.Text = vvp.Size.ToString();
                for (int i = 0; i < vvp.Size; i++)
                {
                    Rectangle rect = CvInvoke.BoundingRectangle(vvp[i]);
                    source.ROI = rect;
                    textImage = source.Copy().Bitmap;
                    source.ROI = Rectangle.Empty;
                    pictureBox2.Image = textImage;
                    await Task.Delay(500);
                }
            }
        }
    }
}