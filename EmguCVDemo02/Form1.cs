﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace EmguCVDemo02
{
    public partial class Form1 : Form
    {
        private Image<Bgr, byte> sourceImage;
        private Image<Gray, byte> grayImage;

        private VideoCapture cam = new VideoCapture();
        private Thread t1;

        public Form1()
        {
            t1 = new Thread(new ThreadStart(camProcess));

            InitializeComponent();
        }

        private void btnOpenImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog()
            {
                Filter = "Image|*.png;*.jpg"
            };

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                sourceImage = new Image<Bgr, byte>(ofd.FileName);
                imageBox1.Image = sourceImage;
            }
        }

        private void btnColor2Gray_Click(object sender, EventArgs e)
        {
            grayImage = sourceImage.Convert<Gray, byte>();
            imageBox2.Image = grayImage;
        }

        private void btnGray2BW_Click(object sender, EventArgs e)
        {
            int bwValue = (int)numBWValue.Value;
            Image<Gray, byte> bwImage = grayImage.ThresholdBinary(new Gray(bwValue), new Gray(255));
            imageBox3.Image = bwImage;
        }

        private void camProcess()
        {
            while (true)
            {
                sourceImage = new Image<Bgr, byte>(cam.QueryFrame().Bitmap);
                imageBox1.Image = sourceImage;
                btnColor2Gray.PerformClick();
                btnGray2BW.PerformClick();
            }
        }

        private void boundingBox(Image<Gray, byte> src, Image<Bgr, byte> draw)
        {
            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            CvInvoke.FindContours(src,
                contours,
                null,
                Emgu.CV.CvEnum.RetrType.External,
                Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);

            for (int i = 0; i < contours.Size; i++)
            {
                VectorOfPoint contour = contours[i];
                Rectangle box = CvInvoke.BoundingRectangle(contour);
                CvInvoke.Rectangle(draw, box, new MCvScalar(255, 0, 255), 3);
            }
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            Image<Bgr, byte> draw = sourceImage.Copy();
            Image<Gray, byte> cannyImage = grayImage.Copy();
            CvInvoke.Canny(grayImage, cannyImage, 255, 255, 5, true);
            boundingBox(cannyImage, draw);
            imageBox2.Image = cannyImage;
            imageBox3.Image = draw;

            //if (!cam.IsOpened)
            //{
            //    cam.Start();
            //}

            //if (t1.IsAlive)
            //{
            //    t1.Abort();
            //}
            //else
            //{
            //    t1.Start();
            //}
        }
    }
}