﻿namespace EmguCVDemo02
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnOpenImage = new System.Windows.Forms.Button();
            this.btnColor2Gray = new System.Windows.Forms.Button();
            this.btnGray2BW = new System.Windows.Forms.Button();
            this.numBWValue = new System.Windows.Forms.NumericUpDown();
            this.imageBox1 = new Emgu.CV.UI.ImageBox();
            this.imageBox2 = new Emgu.CV.UI.ImageBox();
            this.imageBox3 = new Emgu.CV.UI.ImageBox();
            this.btnTest = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numBWValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOpenImage
            // 
            this.btnOpenImage.Location = new System.Drawing.Point(12, 12);
            this.btnOpenImage.Name = "btnOpenImage";
            this.btnOpenImage.Size = new System.Drawing.Size(164, 66);
            this.btnOpenImage.TabIndex = 0;
            this.btnOpenImage.Text = "開啟圖片";
            this.btnOpenImage.UseVisualStyleBackColor = true;
            this.btnOpenImage.Click += new System.EventHandler(this.btnOpenImage_Click);
            // 
            // btnColor2Gray
            // 
            this.btnColor2Gray.Location = new System.Drawing.Point(182, 12);
            this.btnColor2Gray.Name = "btnColor2Gray";
            this.btnColor2Gray.Size = new System.Drawing.Size(164, 66);
            this.btnColor2Gray.TabIndex = 1;
            this.btnColor2Gray.Text = "轉灰階";
            this.btnColor2Gray.UseVisualStyleBackColor = true;
            this.btnColor2Gray.Click += new System.EventHandler(this.btnColor2Gray_Click);
            // 
            // btnGray2BW
            // 
            this.btnGray2BW.Location = new System.Drawing.Point(352, 12);
            this.btnGray2BW.Name = "btnGray2BW";
            this.btnGray2BW.Size = new System.Drawing.Size(164, 66);
            this.btnGray2BW.TabIndex = 2;
            this.btnGray2BW.Text = "二值化";
            this.btnGray2BW.UseVisualStyleBackColor = true;
            this.btnGray2BW.Click += new System.EventHandler(this.btnGray2BW_Click);
            // 
            // numBWValue
            // 
            this.numBWValue.Location = new System.Drawing.Point(522, 37);
            this.numBWValue.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numBWValue.Name = "numBWValue";
            this.numBWValue.Size = new System.Drawing.Size(69, 22);
            this.numBWValue.TabIndex = 3;
            this.numBWValue.Value = new decimal(new int[] {
            127,
            0,
            0,
            0});
            // 
            // imageBox1
            // 
            this.imageBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBox1.Location = new System.Drawing.Point(12, 84);
            this.imageBox1.Name = "imageBox1";
            this.imageBox1.Size = new System.Drawing.Size(288, 422);
            this.imageBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imageBox1.TabIndex = 2;
            this.imageBox1.TabStop = false;
            // 
            // imageBox2
            // 
            this.imageBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBox2.Location = new System.Drawing.Point(306, 84);
            this.imageBox2.Name = "imageBox2";
            this.imageBox2.Size = new System.Drawing.Size(288, 422);
            this.imageBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imageBox2.TabIndex = 4;
            this.imageBox2.TabStop = false;
            // 
            // imageBox3
            // 
            this.imageBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBox3.Location = new System.Drawing.Point(600, 84);
            this.imageBox3.Name = "imageBox3";
            this.imageBox3.Size = new System.Drawing.Size(288, 422);
            this.imageBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imageBox3.TabIndex = 5;
            this.imageBox3.TabStop = false;
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(724, 12);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(164, 66);
            this.btnTest.TabIndex = 6;
            this.btnTest.Text = "Test";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(907, 518);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.imageBox3);
            this.Controls.Add(this.imageBox2);
            this.Controls.Add(this.imageBox1);
            this.Controls.Add(this.numBWValue);
            this.Controls.Add(this.btnGray2BW);
            this.Controls.Add(this.btnColor2Gray);
            this.Controls.Add(this.btnOpenImage);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.numBWValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOpenImage;
        private System.Windows.Forms.Button btnColor2Gray;
        private System.Windows.Forms.Button btnGray2BW;
        private System.Windows.Forms.NumericUpDown numBWValue;
        private Emgu.CV.UI.ImageBox imageBox1;
        private Emgu.CV.UI.ImageBox imageBox2;
        private Emgu.CV.UI.ImageBox imageBox3;
        private System.Windows.Forms.Button btnTest;
    }
}

