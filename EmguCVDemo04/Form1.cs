﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmguCVDemo04
{
    public partial class Form1 : Form
    {
        private Image<Bgr, byte> source;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnLoadImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog()
            {
                Filter = "Image|*.png;*.jpg|All Files|*.*"
            };

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                source = new Image<Bgr, byte>(ofd.FileName);
                imageBox1.Image = source;
            }
        }

        private void btnFindEdge_Click(object sender, EventArgs e)
        {
            Image<Gray, byte> binImg = source.Convert<Gray, byte>()
                .ThresholdBinary(new Gray((int)numBinaryLimit.Value), new Gray(255));
            VectorOfVectorOfPoint vvp = new VectorOfVectorOfPoint();
            Mat hier = new Mat();
            CvInvoke.FindContours(
                binImg,
                vvp,
                hier,
                Emgu.CV.CvEnum.RetrType.External,
                Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple
            );
            Image<Gray, byte> outImg = new Image<Gray, byte>(binImg.Width, binImg.Height, new Gray());
            CvInvoke.DrawContours(outImg, vvp, -1, new MCvScalar(255));
            imageBox2.Image = outImg;
        }
    }
};