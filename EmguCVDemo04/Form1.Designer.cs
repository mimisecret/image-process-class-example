﻿namespace EmguCVDemo04
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.imageBox1 = new Emgu.CV.UI.ImageBox();
            this.btnLoadImage = new System.Windows.Forms.Button();
            this.btnFindEdge = new System.Windows.Forms.Button();
            this.imageBox2 = new Emgu.CV.UI.ImageBox();
            this.numBinaryLimit = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBinaryLimit)).BeginInit();
            this.SuspendLayout();
            // 
            // imageBox1
            // 
            this.imageBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBox1.Location = new System.Drawing.Point(12, 77);
            this.imageBox1.Name = "imageBox1";
            this.imageBox1.Size = new System.Drawing.Size(415, 449);
            this.imageBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imageBox1.TabIndex = 2;
            this.imageBox1.TabStop = false;
            // 
            // btnLoadImage
            // 
            this.btnLoadImage.Location = new System.Drawing.Point(12, 12);
            this.btnLoadImage.Name = "btnLoadImage";
            this.btnLoadImage.Size = new System.Drawing.Size(75, 59);
            this.btnLoadImage.TabIndex = 3;
            this.btnLoadImage.Text = "Load";
            this.btnLoadImage.UseVisualStyleBackColor = true;
            this.btnLoadImage.Click += new System.EventHandler(this.btnLoadImage_Click);
            // 
            // btnFindEdge
            // 
            this.btnFindEdge.Location = new System.Drawing.Point(441, 12);
            this.btnFindEdge.Name = "btnFindEdge";
            this.btnFindEdge.Size = new System.Drawing.Size(75, 59);
            this.btnFindEdge.TabIndex = 4;
            this.btnFindEdge.Text = "輪廓擷取";
            this.btnFindEdge.UseVisualStyleBackColor = true;
            this.btnFindEdge.Click += new System.EventHandler(this.btnFindEdge_Click);
            // 
            // imageBox2
            // 
            this.imageBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBox2.Location = new System.Drawing.Point(441, 77);
            this.imageBox2.Name = "imageBox2";
            this.imageBox2.Size = new System.Drawing.Size(415, 449);
            this.imageBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imageBox2.TabIndex = 5;
            this.imageBox2.TabStop = false;
            // 
            // numBinaryLimit
            // 
            this.numBinaryLimit.Location = new System.Drawing.Point(522, 49);
            this.numBinaryLimit.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numBinaryLimit.Name = "numBinaryLimit";
            this.numBinaryLimit.Size = new System.Drawing.Size(120, 22);
            this.numBinaryLimit.TabIndex = 6;
            this.numBinaryLimit.Value = new decimal(new int[] {
            128,
            0,
            0,
            0});
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1143, 538);
            this.Controls.Add(this.numBinaryLimit);
            this.Controls.Add(this.imageBox2);
            this.Controls.Add(this.btnFindEdge);
            this.Controls.Add(this.btnLoadImage);
            this.Controls.Add(this.imageBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.imageBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBinaryLimit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Emgu.CV.UI.ImageBox imageBox1;
        private System.Windows.Forms.Button btnLoadImage;
        private System.Windows.Forms.Button btnFindEdge;
        private Emgu.CV.UI.ImageBox imageBox2;
        private System.Windows.Forms.NumericUpDown numBinaryLimit;
    }
}

