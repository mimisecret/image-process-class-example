﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace EmguCVDemo03
{
    public partial class Form1 : Form
    {
        private Image<Bgr, byte> sourceImage;
        private Image<Gray, byte> grayImage;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnOpenImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog()
            {
                Filter = "Image|*.png;*.jpg"
            };

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                sourceImage = new Image<Bgr, byte>(ofd.FileName);
                imageBox1.Image = sourceImage;

                DenseHistogram dHisto = new DenseHistogram(256, new RangeF(0, 255));
                dHisto.Calculate<byte>(new Image<Gray, byte>[] { sourceImage[2] }, false, null);
                Mat m = new Mat();
                dHisto.CopyTo(m);
                histoRed.ClearHistogram();
                histoRed.AddHistogram("", Color.Red, m, 256, new float[] { 0, 255 });
                histoRed.Refresh();

                dHisto.Calculate<byte>(new Image<Gray, byte>[] { sourceImage[1] }, false, null);
                dHisto.CopyTo(m);
                histoGreen.ClearHistogram();
                histoGreen.AddHistogram("", Color.Green, m, 256, new float[] { 0, 255 });
                histoGreen.Refresh();

                dHisto.Calculate<byte>(new Image<Gray, byte>[] { sourceImage[0] }, false, null);
                dHisto.CopyTo(m);
                histoBlue.ClearHistogram();
                histoBlue.AddHistogram("", Color.Blue, m, 256, new float[] { 0, 255 });
                histoBlue.Refresh();
            }
        }

        private void btnColor2Gray_Click(object sender, EventArgs e)
        {
            grayImage = sourceImage.Convert<Gray, byte>();
            imageBox2.Image = grayImage;
        }

        private void btnGray2BW_Click(object sender, EventArgs e)
        {
            int bwValue = (int)numBWValue.Value;
            Image<Gray, byte> bwImage = grayImage.ThresholdBinary(new Gray(bwValue), new Gray(255));
            imageBox3.Image = bwImage;
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
        }
    }
}